# suna_read : A crbasic script to read data from a Satlantic SUNA to a cr1000 datalogger

suna_read is a crbasic script that reads and logs full_ascii data from a Satlantic SUNA v2 to a cr1000 
datalogger

See https://www.campbellsci.com/cr1000 for more information on the cr1000.

See http://satlantic.com/suna for more information on the Satlantic SUNA v2.

See examples for package usage

Requirements:
--------------

### Required components###

*cr1000 datalogger
*Satlantic SUNA v2

Example: Read in SUNA v2 Full ascii data and save to a data table
-------------------------------------------------------------------------------
:::crbasic



Installation:
------------

Or you can get the source code from bitbucket
https://bitbucket.org/geofranco/suna_read

::

	$ git clone https://geofranco@bitbucket.org/geofranco/suna_read.git
	$ cd suna_read



Disclaimer:
----------

This software is preliminary or provisional and is subject to revision. It is being provided to meet the need for timely
best science. The software has not received final approval by the U.S. Geological Survey (USGS). No warranty, expressed
or implied, is made by the USGS or the U.S. Government as to the functionality of the software and related material nor
shall the fact of release constitute any such warranty. The software is provided on the condition that neither the USGS
nor the U.S. Government shall be held liable for any damages resulting from the authorized or unauthorized use of the
software.

The USGS provides no warranty, expressed or implied, as to the correctness of the furnished software or the suitability
for any purpose. The software has been tested, but as with any complex software, there could be undetected errors. Users
who find errors are requested to report them to the USGS.

References to non-USGS products, trade names, and (or) services are provided for information purposes only and do not
constitute endorsement or warranty, express or implied, by the USGS, U.S. Department of Interior, or U.S. Government, as
to their suitability, content, usefulness, functioning, completeness, or accuracy.

Although this program has been used by the USGS, no warranty, expressed or implied, is made by the USGS or the United
States Government as to the accuracy and functioning of the program and related program material nor shall the fact of
distribution constitute any such warranty, and no responsibility is assumed by the USGS in connection therewith.

This software is provided "AS IS."


Author(s):
------
John Franco Saraceno <saraceno@usgs.gov>

More information:
-----------------
* CrBasic: http://basiclogging.blogspot.com/2012/03/getting-started-with-crbasic.html
* public domain: https://en.wikipedia.org/wiki/Public_domain
* CC0 1.0: http://creativecommons.org/publicdomain/zero/1.0/
* U.S. Geological Survey: https://www.usgs.gov/
* USGS: https://www.usgs.gov/
* U.S. Geological Survey (USGS): https://www.usgs.gov/
* United States Department of Interior: https://www.doi.gov/
* official USGS copyright policy: http://www.usgs.gov/visual-id/credit_usgs.html#copyright/
* U.S. Geological Survey (USGS) Software User Rights Notice: http://water.usgs.gov/software/help/notice/
* git: https://git-scm.com/
